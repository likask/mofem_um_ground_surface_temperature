# MoFEM is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# MoFEM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MoFEM. If not, see <http://www.gnu.org/licenses/>

add_executable(ground_surface_temperature
  ${CMAKE_CURRENT_SOURCE_DIR}/ground_surface_temperature.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/../third_party/spa.c)

target_link_libraries(ground_surface_temperature
  users_modules
  mofem_finite_elements
  mofem_interfaces
  mofem_multi_indices
  mofem_petsc
  mofem_approx
  mofem_third_party
  mofem_cblas
  ${MoFEM_PROJECT_LIBS})

cm_export_file("Valey2.cub" export_files_ground_surface_temperature)
add_test(ground_surface_temperature ${MoFEM_MPI_RUN} 
  ${MPI_RUN_FLAGS} -np 1 
  ${CMAKE_CURRENT_BINARY_DIR}/ground_surface_temperature -my_file Valey2.cub)

cm_export_file("parameters.in" export_files_ground_surface_temperature)
add_executable(climate_model
  ${CMAKE_CURRENT_SOURCE_DIR}/climate_model.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/../third_party/spa.c)
target_link_libraries(climate_model
  users_modules
  mofem_finite_elements
  mofem_interfaces
  mofem_multi_indices
  mofem_petsc
  mofem_approx
  mofem_third_party
  mofem_cblas
  ${MoFEM_PROJECT_LIBS})

add_test(climate_model ${MoFEM_MPI_RUN} ${MPI_RUN_FLAGS} -np 1
  ${CMAKE_CURRENT_BINARY_DIR}/climate_model -my_nb_days 2 -my_step_size 0.02)
add_test(climate_model_compare ${CMAKE_COMMAND} -E compare_files
  ${CMAKE_CURRENT_SOURCE_DIR}/blessed_files/time_variation_model.txt
  ${CMAKE_CURRENT_BINARY_DIR}/time_variation_model.txt)
