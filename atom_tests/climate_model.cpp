/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <MoFEM.hpp>
using namespace MoFEM;

#include <MethodForForceScaling.hpp>
#include <DirichletBC.hpp>

#include <Projection10NodeCoordsOnField.hpp>

#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector.hpp>

#include <moab/AdaptiveKDTree.hpp>
extern "C" {
  #include <spa.h>
}

#include <time.h>
//#include <ThermalElement.hpp>
#include<moab/Skinner.hpp>

#include <GenericClimateModel.hpp>
#include <GroundSurfaceTemerature.hpp>

#include <boost/program_options.hpp>
#include <iostream>
#include <fstream>
#include <iterator>

#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>
#include <fstream>
#include <iostream>

namespace bio = boost::iostreams;
using bio::tee_device;
using bio::stream;

#include <CrudeClimateModel.hpp>

static char help[] =
  "-nb_days [days]\n"
  "-my_step_size [days]\n\n";

int main(int argc, char *argv[]) {

  MoFEM::Core::Initialize(&argc,&argv,(char *)0,help);

  try {

  PetscBool flg;
  PetscScalar nb_days;
  CHKERR PetscOptionsGetScalar(PETSC_NULL,"-my_nb_days",&nb_days,&flg);
  if(flg != PETSC_TRUE) {
    nb_days = 365;
  }
  PetscScalar time_step;
  CHKERR PetscOptionsGetScalar(PETSC_NULL,"-my_step_size",&time_step,&flg);
  if(flg != PETSC_TRUE) {
    time_step = 0.2; // 5th part of day
  }

  CrudeClimateModel time_data("parameters.in");
  CHKERR time_data.testCode(60*60*24*nb_days,60*60*24*time_step);

  } CATCH_ERRORS;

  MoFEM::Core::Finalize();
  return 0;
}
