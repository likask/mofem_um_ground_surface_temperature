/*
 * This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __GenericClimateModel_HPP
#define __GenericClimateModel_HPP

struct GenericClimateModel {

  double T0; // reference temperature (K)
  double e0; // reference saturation vapor pressure (es at a certain temp, usually 0 deg C) (Pa)
  double Rv; // gas constant for water vapor (J*K/Kg)
  double Lv; // latent heat of vaporization of water (J)

  double u10;		//< wind at high 10m (m/s)
  double CR; 		//< cloudiness factor (0–1, dimensionless)
  double Ta;		//< air temperature (C)
  double Td;		//< dew point temperature (C)
  double P;		  //< pressure
  double Rs; 		//< observed solar radiation (W/m2)

  double zenith;       //topocentric zenith angle [degrees]
  double azimuth;      //topocentric azimuth angle (eastward from north) [for navigators and solar radiation]

  /** \brief Clausius-Clapeyron equation
    */
  double calculateVapourPressureClausiusClapeyron(double T) {
    return e0*exp((Lv/Rv)*((1./T0)-(1./(T+T0))));
  }

  double calculateVapourPressureTetenFormula(double T) {
    const double b = 17.2694;
    const double T2 = 35.86;
    return e0*exp(b*T/(T+T0-T2));
  }

  double calculateVapourPressureTetenFormula_dT(double T) {
    const double b = 17.2694;
    const double T2 = 35.86;
    return e0*exp(b*T/(T+T0-T2))*(b/(T+T0-T2) - b*T/pow(T+T0-T2,2));
  }

  double calculateVapourPressure(double T) {
    //This use Tetent's formula by default
    return calculateVapourPressureTetenFormula(T);
  }

  double calculateVapourPressure_dT(double T) {
    //This use Tetent's formula by default
    return calculateVapourPressureTetenFormula_dT(T);
  }

  double calculateMixingRatio(double T,double P) {
    const double eps = 0.622;
    double e = calculateVapourPressure(T);
    return eps*e/(P-e);
  }

  double calculateMixingRatio_dT(double T,double P) {
    const double eps = 0.622;
    double e = calculateVapourPressure(T);
    double e_dT = calculateVapourPressure_dT(T);
    return eps*e_dT/(P-e) + eps*e*e_dT/pow(P-e,2);
  }

  double calculateAbsoluteVirtualTempertaure(double T,double Td,double P) {
    double r = calculateMixingRatio(Td,P);
    return T*(0.61+r);
  }

  double calculateAbsoluteVirtualTempertaure_dT(double T,double Td,double P) {
    double r = calculateMixingRatio(Td,P);
    return (0.61+r);
  }

  GenericClimateModel() {

    T0 = 273.15; // reference temperature (K)
    e0 = 611.3; // reference saturation vapor pressure (es at a certain temp, usually 0 deg C) (Pa)
    Rv = 461.5; // gas constant for water vapor (J*K/Kg)
    Lv = 2.5e6; // latent heat of vaporization of water (J)

    u10 = 2.7;			//< wind at high 10m (m/s)
    CR = 0; 			//< cloudness factor (0–1, dimensionless)
    Ta = 10;			//< air temperature (C)
    Td = 5;			//< dew point temperature (C)
    P = 101325;		//< pressure
    Rs = 1361; 		//< observed solar radiation (W/m2)

    zenith = 0;       //topocentric zenith angle [degrees]
    azimuth = 0;      //topocentric azimuth angle (eastward from north) [for navigators and solar radiation]

  }

  virtual MoFEMErrorCode set(double t = 0) = 0;
};

#endif //__GenericClimateModel_HPP
